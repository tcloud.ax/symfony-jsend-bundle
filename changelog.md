# Symfony jsend bundle

## Version 3.0.1

symfony/http-foundation downgraded to ^6.0

symfony/config downgraded to ^6.0

symfony/dependency-injection downgraded to ^6.0

symfony/http-kernel downgraded to ^6.0

## Version 2.0.1

symfony/http-foundation downgraded to ^5.0

symfony/config downgraded to ^5.0

symfony/dependency-injection downgraded to ^5.0

symfony/http-kernel downgraded to ^5.0
