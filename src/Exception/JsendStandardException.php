<?php

declare(strict_types=1);

namespace JsendStandard\Exception;

use RuntimeException;

/**
 * @author tcloud <tcloud.ax@gmail.com>
 * @since  v1.0.0
 */
class JsendStandardException extends RuntimeException
{
}
